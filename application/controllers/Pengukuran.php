<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengukuran extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->model("pengukuran_model");
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$data['konten'] = "dashboard";
		$this->load->view('template/dashborad', $data);
	}
	public function simpan()
	{
		$pengukuran = $this->pengukuran_model;

		$simpan = $pengukuran->insert();
		if ($simpan == 1) {
			$return = array(
				'code' => 200,
				'msg' => 'Data Berhasil Disimpan'
			);
		} else {
			$return = array(
				'code' => 202,
				'msg' => 'Data Gagal Disimpan'
			);
		}
		die(json_encode($return));
	}
	public function simpan_edit()
	{
		$pengukuran = $this->pengukuran_model;

		$simpan = $pengukuran->simpan_edit();
		if ($simpan == 1) {
			$return = array(
				'code' => 200,
				'msg' => 'Data Berhasil Diupdate'
			);
		} else {
			$return = array(
				'code' => 202,
				'msg' => 'Data Gagal Diupdate'
			);
		}
		die(json_encode($return));
	}
}
