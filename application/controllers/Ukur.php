<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ukur extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model("pengukuran_model");
    }

    public function index()
    {
        $data['title'] = "Lutut";
        $data['konten'] = "Lutut";
        $this->load->view('page/lutut', $data);
    }
    public function ulna()
    {
        $data['title'] = "Ulna";
        $data['konten'] = "Ulna";
        $this->load->view('page/ulna', $data);
    }
    public function laporan()
    {
        // print_r($_GET);
        if (isset($_GET['awal']) && isset($_GET['akhir']) && isset($_GET['jenis'])) {
            $data['laporan'] = $this->pengukuran_model->laporan($_GET);
        } else {
            $data['laporan'] = [];
        }
        $data['title'] = "Laporan";
        $data['konten'] = "Laporan";
        $this->load->view('page/laporan', $data);
    }
    public function export()
    {
        $data = $this->pengukuran_model->laporan($_GET);

        include_once APPPATH . '/third_party/xlsxwriter.class.php';
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "Laporan.xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $styles = array('font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'fill' => '#eee', 'halign' => 'center', 'border' => 'left,right,top,bottom');
        $styles2 = array('font' => 'Arial', 'font-size' => 10, 'font-style' => 'bold', 'halign' => 'center', 'border' => 'left,right,top,bottom');
        $jenis = $_GET['jenis'] == 1 ? 'Tinggi Lutut' : 'Panjang Ulna';
        $header = array(
            'No' => 'integer',
            'Nama Pasien' => 'string',
            'No.Rm	' => 'string',
            'Tgl Lahir' => 'string',
            'Tgl Ukur' => 'string',
            'Jenis Kelamin' => 'string',
            $jenis => 'string',
            'Berat Badan Ideal(KG)' => 'string',
            'Tinggi Estimasi(CM)' => 'string',
            'Kondisi Pasien' => 'string',
            'Energi (kkal)' => 'string',
            'Protein (gram)' => 'string',
            'Lemak (gram)' => 'string',
            'Karbo (gram)' => 'string',
        );

        $writer = new XLSXWriter();
        $writer->setAuthor('Montazi');

        $writer->writeSheetHeader('Sheet1', $header, $styles);
        $no = 1;
        foreach ($data as $row) {
            $writer->writeSheetRow(
                'Sheet1',
                [
                    $no,
                    $row->nama,
                    $row->norm,
                    tgl_indo($row->tgl_lahir),
                    tgl_indo($row->tanggal),
                    $row->jenis_kelamin,
                    ($_GET['jenis'] == 1 ? $row->tinngi_lutut : $row->panjang_ulna),
                    $row->bbi,
                    $row->tb,
                    ($row->kondisi == 1 ? 'Non - Critical Ill Patients' : 'Critical Ill Patients'),
                    $row->kebutuhan_gizi,
                    $row->h_protein,
                    $row->h_lemak,
                    $row->h_karbo,
                ],
                $styles2
            );
            $no++;
        }
        $writer->writeToStdOut();
    }
    public function data()
    {
        // print_r($_GET);
        if (isset($_GET['awal']) && isset($_GET['akhir']) && isset($_GET['jenis'])) {
            $data['laporan'] = $this->pengukuran_model->laporan($_GET);
        } else {
            $data['laporan'] = [];
        }
        $data['title'] = "Laporan";
        $data['konten'] = "Laporan";
        $this->load->view('page/datalaporan', $data);
    }
    public function hapus()
    {
        $id = $this->input->post('id');
        $hapus = $this->pengukuran_model->hapus($id);
        // print_r($hapus);
        // exit;
        if ($hapus == 1) {
            $return = array(
                'code' => 200,
                'msg' => 'Data Berhasil Dihapus'
            );
        } else {
            $return = array(
                'code' => 202,
                'msg' => 'Data Gagal Dihapus'
            );
        }

        die(json_encode($return));
    }
    public function edit($id)
    {
        $dt = $this->pengukuran_model->edit($id);
        $data['data'] = $dt;
        $data['title'] = "edit";
        $data['konten'] = "edit";
        if ($dt[0]->jenis == 'lutut') {
            $this->load->view('page/lutut-edit', $data);
        } else {
            $this->load->view('page/ulna-edit', $data);
        }
    }
}
