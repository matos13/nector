<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pasien_model extends CI_Model
{
    private $_table = "pasien";

    public $id;
    public $nama;
    public $norm;
    public $tgl_lahir;
    public $domisili;

    var $column_order = array(null, 'nama', 'jenis_kelamin', 'norm', 'tgl_lahir', 'domisili');

    var $column_search = array('nama', 'jenis_kelamin', 'norm', 'tgl_lahir', 'domisili');
    // default order 
    var $order = array('id' => 'asc');

    public function rules()
    {
        return [
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],

            [
                'field' => 'norm',
                'label' => 'No.Rm',
                'rules' => 'required'
            ],
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }
    public function getByNorm($norm)
    {
        return $this->db->get_where($this->_table, ["norm" => $norm])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        // print_r($post);
        // exit;
        // $this->id = uniqid();
        $tgl = $post["tgl_lahir"];
        $tgl = explode('/', $tgl);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
        $this->nama = $post["nama"];
        $this->norm = $post["norm"];
        $this->jenis_kelamin = $post["jenis_kelamin"];
        $this->tgl_lahir = $tgl;
        $this->domisili = $post["domisili"];
        // print_r($this);
        // exit;
        return $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();

        $tgl = $post["tgl_lahir"];
        $tgl = explode('/', $tgl);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];

        $this->id = $post["id"];
        $this->nama = $post["nama"];
        $this->norm = $post["norm"];
        $this->jenis_kelamin = $post["jenis_kelamin"];
        $this->tgl_lahir = $tgl;
        $this->domisili = $post["domisili"];
        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }
    public function hapus()
    {
        $post = $this->input->post();
        $data = [
            'flag_hapus' => 'Y',
        ];
        $this->db->where('id', $post["id"]);
        return  $this->db->update($this->_table, $data);
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }


    private function _get_datatables_query()
    {
        $this->db->from($this->_table);
        $this->db->where('flag_hapus', 'N');
        $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->_table);
        return $this->db->count_all_results();
    }
}
