<?php defined('BASEPATH') or exit('No direct script access allowed');

class Pengukuran_model extends CI_Model
{
    private $_table = "pengukuran";

    public $id;
    public $id_pasien;
    public $tanggal;
    public $jenis;
    public $tinngi_lutut;
    public $bbi;
    public $tb;
    public $kondisi;
    public $kebutuhan_gizi;
    public $panjang_ulna;
    public $f_aktivitas;
    public $f_stress;
    public $n_stress;
    public $bmr;
    public $kkal;
    public $protein;
    public $lemak;
    public $karbo;
    public $h_protein;
    public $h_lemak;
    public $h_karbo;

    public function insert()
    {
        $post = $this->input->post();

        //cek pasien
        $cek_pasien = $this->db->query("SELECT * from pasien where norm = $post[rm] ")->result();
        if (empty($cek_pasien)) {
            $tanggal = $post["tgl"];
            $tgl_lahir = explode('/', $tanggal);
            $tgl_lahir = $tgl_lahir[2] . '-' . $tgl_lahir[1] . '-' . $tgl_lahir[0];
            $data = [
                'norm' =>  $post["rm"],
                'nama' => $post["pasien"],
                'jenis_kelamin' => $post["radio"],
                'tgl_lahir' => $tgl_lahir

            ];
            $this->db->insert('pasien', $data);
            $id_pasien = $this->db->insert_id();
        } else {
            $id_pasien = $cek_pasien[0]->id;
        }

        // print_r($post);
        // exit;
        $tanggal = $post["tgl_ukur"];
        $tgl = explode('/', $tanggal);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];

        if ($post["jenis"] == 'lutut') {
            $this->tinngi_lutut = $post["lutut"];
        } else {
            $this->panjang_ulna = $post["ulna"];
        }
        $this->id_pasien = $id_pasien;
        $this->jenis = $post["jenis"];
        $this->tanggal = $tgl;
        $this->bbi = $post["bbideal"];
        $this->tb = $post["hasil"];
        $this->kondisi = $post["metode"];
        $this->kebutuhan_gizi = $post["kebgizi"];
        $this->f_aktivitas = $post["aktivitas"];
        $this->f_stress = $post["stress"];
        $this->f_stress = $post["stress"];
        $this->n_stress = $post["nilai"];
        $this->bmr = $post["bmr"];
        $this->kkal = $post["kkal"];
        $this->protein = $post["protein"];
        $this->lemak = $post["lemak"];
        $this->karbo = $post["karbo"];
        $this->h_protein = $post["h_protein"];
        $this->h_lemak = $post["h_lemak"];
        $this->h_karbo = $post["h_karbo"];

        return $this->db->insert($this->_table, $this);
    }
    public function laporan($param)
    {
        $awal = tgl_sql($param['awal']);
        $akhir = tgl_sql($param['akhir']);
        $jenis = $param['jenis'] == 1 ? 'lutut' : 'ulna';
        $query = "SELECT pengukuran.*,pasien.nama,pasien.norm,pasien.jenis_kelamin,pasien.tgl_lahir FROM pengukuran join pasien on pasien.id = pengukuran.id_pasien 
        WHERE (pengukuran.tanggal BETWEEN '$awal' AND '$akhir') and pengukuran.jenis ='$jenis' order by pengukuran.tanggal asc";
        // print_r($query);
        return $this->db->query($query)->result();
    }
    public function hapus($id)
    {
        // print($id);
        $this->db->where('id', $id);
        return $this->db->delete($this->_table);
    }
    public function edit($id)
    {
        $query = "SELECT pengukuran.*,pasien.nama,pasien.norm,pasien.jenis_kelamin,pasien.tgl_lahir FROM pengukuran join pasien on pasien.id = pengukuran.id_pasien 
        WHERE pengukuran.id = $id ";
        // print_r($query);
        return $this->db->query($query)->result();
    }
    public function simpan_edit()
    {
        $post = $this->input->post();

        //cek pasien
        $cek_pasien = $this->db->query("SELECT * from pasien where norm = $post[rm] ")->result();
        if (empty($cek_pasien)) {
            $tanggal = $post["tgl"];
            $tgl_lahir = explode('/', $tanggal);
            $tgl_lahir = $tgl_lahir[2] . '-' . $tgl_lahir[1] . '-' . $tgl_lahir[0];
            $data = [
                'norm' =>  $post["rm"],
                'nama' => $post["pasien"],
                'jenis_kelamin' => $post["radio"],
                'tgl_lahir' => $tgl_lahir

            ];
            $this->db->insert('pasien', $data);
            $id_pasien = $this->db->insert_id();
        } else {
            $tanggal = $post["tgl"];
            $tgl_lahir = explode('/', $tanggal);
            $tgl_lahir = $tgl_lahir[2] . '-' . $tgl_lahir[1] . '-' . $tgl_lahir[0];
            $data = [
                'norm' =>  $post["rm"],
                'nama' => $post["pasien"],
                'jenis_kelamin' => $post["radio"],
                'tgl_lahir' => $tgl_lahir

            ];
            $this->db->update('pasien', $data, array('id' => $cek_pasien[0]->id));
            $id_pasien = $cek_pasien[0]->id;
        }

        // print_r($post);
        // exit;
        $tanggal = $post["tgl_ukur"];
        $tgl = explode('/', $tanggal);
        $tgl = $tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];

        if ($post["jenis"] == 'lutut') {
            $this->tinngi_lutut = $post["lutut"];
        } else {
            $this->panjang_ulna = $post["ulna"];
        }
        $this->id_pasien = $id_pasien;
        $this->jenis = $post["jenis"];
        $this->id = $post["id"];
        $this->tanggal = $tgl;
        $this->bbi = $post["bbideal"];
        $this->tb = $post["hasil"];
        $this->kondisi = $post["metode"];
        $this->kebutuhan_gizi = $post["kebgizi"];
        $this->f_aktivitas = $post["aktivitas"];
        $this->f_stress = $post["stress"];
        $this->f_stress = $post["stress"];
        $this->n_stress = $post["nilai"];
        $this->bmr = $post["bmr"];
        $this->kkal = $post["kkal"];
        $this->protein = $post["protein"];
        $this->lemak = $post["lemak"];
        $this->karbo = $post["karbo"];
        $this->h_protein = $post["h_protein"];
        $this->h_lemak = $post["h_lemak"];
        $this->h_karbo = $post["h_karbo"];

        return $this->db->update($this->_table, $this, array('id' => $post['id']));
    }
}
