<?php $this->load->view('template/header');

// dd($laporan);
$awal = isset($_GET['awal']) ? $_GET['awal'] : date('d/m/Y');
$akhir = isset($_GET['akhir']) ? $_GET['akhir'] : date('d/m/Y');
$jenis = isset($_GET['jenis']) ? $_GET['jenis'] : '';
?>
<style>
    .table-responsive {
        display: table;
        display: block !important;
    }
</style>
<span style="color: #000;">Data Pengukuran</span>
<br>
<form action="" method="get">

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-6">
            <div class="form-group">
                <label for="tgl">Tanggal</label>
                <div class="input-group">
                    <input id="awal" name="awal" type="text" class="form-control" value="<?php echo $awal ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                    <input id="akhir" name="akhir" type="text" class="form-control" value="<?php echo $akhir ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="lutut">Metode</label>
                <div class="input-group">
                    <select name="jenis" id="jenis" class="form-control">
                        <option value="1" <?php echo ($jenis == 1 ? 'selected' : '') ?>>Tinggi Lutut</option>
                        <option value="2" <?php echo ($jenis == 2 ? 'selected' : '') ?>>Panjang Ulna</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="hidden" name="cari" value="cari">
                <button name="button" type="submit" class="btn btn-primary">Preview</button>
                <button name="button" type="button" class="btn btn-warning" onclick="history.back()">Batal</button>
            </div>
        </div>
</form>

<?php
if (isset($_GET['cari'])) :
?>
    <div class="col-md-12 table-responsive" style="overflow-x:auto;">
        <?php if ($jenis == 1) : ?>
            <center>
                <h4>DATA PENGUKURAN TINGGI LUTUT</h4>
            </center>
        <?php else : ?>
            <center>
                <h4>DATA PENGUKURAN PANJANG ULNA</h4>
            </center>
        <?php endif; ?>
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Nama Pasien</th>
                    <th scope="col">NO RM</th>
                    <th scope="col">TGL LAHIR</th>
                    <th scope="col">TGL UKUR</th>
                    <th scope="col">JENIS KELAMIN</th>
                    <?php if ($jenis == 1) : ?>
                        <th scope="col">TINGGI LUTUT</th>
                    <?php else : ?>
                        <th scope="col">PANJANG ULNA</th>
                    <?php endif; ?>
                    <th scope="col">BERAT BADAN IDEAL (KG)</th>
                    <th scope="col">TINGGI ESTIMASI (CM)</th>
                    <th scope="col">KONDISI PASIEN</th>
                    <th scope="col">KEBUTUHAN GIZI (KKAL)</th>
                    <th>TINDAKAN</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($laporan as $key => $value) : ?>
                    <tr>
                        <td align="center"><?php echo $key + 1 ?></td>
                        <td align="left"><?php echo $value->nama ?></td>
                        <td align="center"><?php echo $value->norm ?></td>
                        <td align="center"><?php echo tgl_indo($value->tgl_lahir) ?></td>
                        <td align="center"><?php echo tgl_indo($value->tanggal) ?></td>
                        <td align="center"><?php echo $value->jenis_kelamin ?></td>
                        <?php if ($jenis == 1) : ?>
                            <td align="center"><?php echo $value->tinngi_lutut ?></td>
                        <?php else : ?>
                            <td align="center"><?php echo $value->panjang_ulna ?></td>
                        <?php endif; ?>
                        <td align="center"><?php echo $value->bbi ?></td>
                        <td align="center"><?php echo $value->tb ?></td>
                        <td align="left"><?php echo ($value->kondisi == 1 ? 'Non - Critical Ill Patients' : 'Critical Ill Patients') ?></td>
                        <td align="center"><?php echo $value->kebutuhan_gizi ?></td>
                        <td>
                            <a class="btn btn-success btn-sm" onclick="edit(<?php echo $value->id ?>)"><i class="fa fa-edit"></i> </a>
                            <a class="btn btn-danger btn-sm" onclick="hapus(<?php echo $value->id ?>)"><i class="fa fa-trash"></i> </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
</div>

<?php $this->load->view('template/footer'); ?>
<script>
    $('#awal').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#akhir').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    function excel() {
        window.location.href = '<?= base_url('/ukur/export?awal='); ?><?php echo ($awal) ?>&akhir=<?php echo ($akhir) ?>&jenis=<?php echo ($jenis) ?>';
    }

    function hapus(id) {
        swal({
                title: "Apakah anda yakin?",
                text: "Data tidak dapat dikembalikan!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/ukur/hapus",
                        data: {
                            id: id
                        },
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    location.reload();
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });
    }
    function edit(id){
        window.location.href = '<?= base_url('/ukur/edit/'); ?>' + id;
    }
</script>