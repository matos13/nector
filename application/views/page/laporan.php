<?php $this->load->view('template/header');

// print_r($laporan)
$awal = isset($_GET['awal']) ? $_GET['awal'] : date('d/m/Y');
$akhir = isset($_GET['akhir']) ? $_GET['akhir'] : date('d/m/Y');
$jenis = isset($_GET['jenis']) ? $_GET['jenis'] : '';
?>
<style>
    .table-responsive {
        display: table;
        display: block !important;
    }
</style>
<span style="color: #000;">LAPORAN</span>
<br>
<form action="" method="get">

    <div class="row" style="margin-top: 2%;">
        <div class="col-md-6">
            <div class="form-group">
                <label for="tgl">Tanggal</label>
                <div class="input-group">
                    <input id="awal" name="awal" type="text" class="form-control" value="<?php echo $awal ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                    <input id="akhir" name="akhir" type="text" class="form-control" value="<?php echo $akhir ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="lutut">Metode</label>
                <div class="input-group">
                    <select name="jenis" id="jenis" class="form-control">
                        <option value="1" <?php echo ($jenis == 1 ? 'selected' : '') ?>>Tinggi Lutut</option>
                        <option value="2" <?php echo ($jenis == 2 ? 'selected' : '') ?>>Panjang Ulna</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="hidden" name="cari" value="cari">
                <button name="button" type="submit" class="btn btn-primary">Preview</button>
                <button name="button" type="button" class="btn btn-warning" onclick="history.back()">Batal</button>
            </div>
        </div>
</form>

<?php
if (isset($_GET['cari'])) :
?>
    <div class="col-md-12 table-responsive" style="overflow-x:auto;">
        <div style="margin-bottom: 10px;">
            <a class="btn btn-success btn-sm" onclick="excel()" style="color:#fff"><i class="fa fa-file-excel"></i> Download excel</a>
        </div>
        <?php if ($jenis == 1) : ?>
            <center>
                <h4>LAPORAN PENGUKURAN TINGGI LUTUT</h4>
            </center>
        <?php else : ?>
            <center>
                <h4>LAPORAN PENGUKURAN PANJANG ULNA</h4>
            </center>
        <?php endif; ?>
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                    <th scope="col" rowspan="2">No</th>
                    <th scope="col" rowspan="2">Nama Pasien</th>
                    <th scope="col" rowspan="2">NO RM</th>
                    <th scope="col" rowspan="2">TGL LAHIR</th>
                    <th scope="col" rowspan="2">TGL UKUR</th>
                    <th scope="col" rowspan="2">JENIS KELAMIN</th>
                    <?php if ($jenis == 1) : ?>
                        <th scope="col" rowspan="2">TINGGI LUTUT</th>
                    <?php else : ?>
                        <th scope="col" rowspan="2">PANJANG ULNA</th>
                    <?php endif; ?>
                    <th scope="col" rowspan="2">BERAT BADAN IDEAL (KG)</th>
                    <th scope="col" rowspan="2">TINGGI ESTIMASI (CM)</th>
                    <th scope="col" rowspan="2">KONDISI PASIEN</th>
                    <th scope="col" colspan="4" style="text-align:center">KEBUTUHAN GIZI</th>
                </tr>
                <tr>
                    <th scope="col">ENERGI (kkal)</th>
                    <th scope="col">PROTEIN (gram)</th>
                    <th scope="col">LEMAK (gram)</th>
                    <th scope="col">KARBOHIDRAT (gram)</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($laporan as $key => $value) : ?>
                    <tr>
                        <td align="center"><?php echo $key + 1 ?></td>
                        <td align="left"><?php echo $value->nama ?></td>
                        <td align="center"><?php echo $value->norm ?></td>
                        <td align="center"><?php echo tgl_indo($value->tgl_lahir) ?></td>
                        <td align="center"><?php echo tgl_indo($value->tanggal) ?></td>
                        <td align="center"><?php echo $value->jenis_kelamin ?></td>
                        <?php if ($jenis == 1) : ?>
                            <td align="center"><?php echo $value->tinngi_lutut ?></td>
                        <?php else : ?>
                            <td align="center"><?php echo $value->panjang_ulna ?></td>
                        <?php endif; ?>
                        <td align="center"><?php echo $value->bbi ?></td>
                        <td align="center"><?php echo $value->tb ?></td>
                        <td align="left"><?php echo ($value->kondisi == 1 ? 'Non - Critical Ill Patients' : 'Critical Ill Patients') ?></td>
                        <td align="center"><?php echo $value->kebutuhan_gizi ?></td>
                        <td align="center"><?php echo $value->h_protein ?></td>
                        <td align="center"><?php echo $value->h_lemak ?></td>
                        <td align="center"><?php echo $value->h_karbo ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endif; ?>
</div>

<?php $this->load->view('template/footer'); ?>
<script>
    $('#awal').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#akhir').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    function excel() {
        window.location.href = '<?= base_url('/ukur/export?awal='); ?><?php echo ($awal) ?>&akhir=<?php echo ($akhir) ?>&jenis=<?php echo ($jenis) ?>';
    }
</script>