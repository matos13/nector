<?php $this->load->view('template/header');
$nilai_stress = [];
if ($data[0]->f_stress == 1) {
    $nilai_stress = ['1.15', '1.2'];
} elseif ($data[0]->f_stress == 2) {
    $nilai_stress = ['1.13'];
} elseif ($data[0]->f_stress == 3) {
    $nilai_stress = ['1.13', '1.20', '1.25', '1.30', '1.35'];
} elseif ($data[0]->f_stress == 4) {
    $nilai_stress = ['1.3', '1.35', '1.40', '1.45', '1.50'];
} elseif ($data[0]->f_stress == 5) {
    $nilai_stress = ['1.35', '1.40', '1.45', '1.50', '1.55'];
} elseif ($data[0]->f_stress == 6) {
    $nilai_stress = ['1.5'];
} elseif ($data[0]->f_stress == 7) {
    $nilai_stress = ['1.5', '1.55', '1.60', '1.65', '1.70', '1.75', '1.80'];
} elseif ($data[0]->f_stress == 8) {
    $nilai_stress = ['1.1', '1.15', '1.20', '1.25', '1.30', '1.35', '1.40', '1.45', '1.50'];
} elseif ($data[0]->f_stress == 9) {
    $nilai_stress = ['1.20', '1.25', '1.30', '1.35', '1.40'];
} elseif ($data[0]->f_stress == 10) {
    $nilai_stress = ['1.1', '1.15', '1.20', '1.25'];
} elseif ($data[0]->f_stress == 11) {
    $nilai_stress = ['1.25', '1.30', '1.35', '1.40', '1.45', '1.50'];
} elseif ($data[0]->f_stress == 12) {
    $nilai_stress = ['1.50', '1.55', '1.60', '1.65', '1.70', '1.75', '1.80', '1.85', '1.90', '1.95', '2'];
} elseif ($data[0]->f_stress == 13) {
    $nilai_stress = ['1.3'];
}

?>
<span style="color: #000;">PERHITUNGAN ESTIMASI TINGGI BADAN MENGGUNAKAN PANJANG ULNA</span>
<br>
<form id="form">
    <input type="hidden" name="jenis" value="ulna">
    <input type="hidden" name="id" value="<?php echo $data[0]->id ?>">
    <div class="row" style="margin-top: 2%;">
        <div class="col-md-6">
            <div class="form-group">
                <label for="lutut">Nama Pasien</label>
                <div class="input-group">
                    <input id="pasien" name="pasien" type="text" class="form-control" value="<?php echo $data[0]->nama ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="lutut">No.Rm</label>
                <div class="input-group">
                    <input id="rm" name="rm" type="text" class="form-control" value="<?php echo $data[0]->norm ?>">
                </div>
            </div>
            <div class="form-group">
                <label for="tgl">Tanggal Ukur</label>
                <div class="input-group">
                    <input id="tgl_ukur" name="tgl_ukur" type="text" class="form-control" value="<?php echo tgl_dis($data[0]->tanggal) ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Jenis Kelamin</label>
                <div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input name="radio" id="radio_0" type="radio" class="custom-control-input" value="L" <?php echo ($data[0]->jenis_kelamin == 'L' ? 'checked' : '') ?>>
                        <label for="radio_0" class="custom-control-label">Laki-laki</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                        <input name="radio" id="radio_1" type="radio" class="custom-control-input" value="P" <?php echo ($data[0]->jenis_kelamin == 'P' ? 'checked' : '') ?>>
                        <label for="radio_1" class="custom-control-label">Perempuan</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="tgl">Tanggal Lahir</label>
                <div class="input-group">
                    <input id="tgl" name="tgl" type="text" class="form-control" value="<?php echo tgl_dis($data[0]->tgl_lahir) ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="fa fa-calendar-o"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="lutut">Panjang Ulna</label>
                <div class="input-group">
                    <select name="ulna" id="ulna" class="form-control">
                        <?php for ($i = 18.5; $i <= 32; $i += 0.5) {
                        ?>
                            <option value="<?php echo $i ?>" <?php echo ($data[0]->panjang_ulna == $i ? 'selected' : '') ?>><?php echo $i ?></option>
                        <?php
                            // $i += 0.5;
                        } ?>
                    </select>
                    <div class="input-group-append">
                        <div class="input-group-text">Cm</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button name="button" type="button" class="btn btn-primary" onclick="hitung()">Hitung</button>
            </div>

        </div>
        <div class="col-md-6" id="div2">
            <div class="form-group">
                <label for="bbideal">Berat Badan Ideal</label>
                <div class="input-group">
                    <input id="bbideal" name="bbideal" type="text" class="form-control" value="<?php echo ($data[0]->bbi) ?>" readonly>
                    <div class="input-group-append">
                        <div class="input-group-text">Kg</div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="hasil">Estimasi Tinggi Badan</label>
                <input id="hasil" name="hasil" type="text" class="form-control" value="<?php echo ($data[0]->tb) ?>" readonly>
                <input type="hidden" name="usia" id="usia">
            </div>
            <div>
                <div class="form-group">
                    <label for="lutut">Pasien</label>
                    <div class="input-group">
                        <select name="metode" id="metode" class="form-control" onchange="rubahmetode(this)">
                            <option value="1" <?php echo ($data[0]->kondisi == '1' ? 'selected' : '') ?>>Non - Critical Ill Patients</option>
                            <option value="2" <?php echo ($data[0]->kondisi == '2' ? 'selected' : '') ?>>Critical Ill Patients</option>
                        </select>
                    </div>
                </div>
            </div>
            <div id="non" style="display: <?php echo $data[0]->kondisi == '1' ? 'block' : 'none' ?>;">
                <div class="form-group">
                    <label for="lutut">Faktor Aktivitas</label>
                    <div class="input-group">
                        <select name="aktivitas" id="aktivitas" class="form-control">
                            <option value="1.05" <?php echo ($data[0]->f_aktivitas == '1.05' ? 'selected' : '') ?>>Total Bed Rest, CVA-ICH</option>
                            <option value="1.1" <?php echo ($data[0]->f_aktivitas == '1.1' ? 'selected' : '') ?>>Mobilisasi di tempat tidur</option>
                            <option value="1.2" <?php echo ($data[0]->f_aktivitas == '1.2' ? 'selected' : '') ?>>Jalan di sekitar kamar</option>
                            <option value="1.3" <?php echo ($data[0]->f_aktivitas == '1.3' ? 'selected' : '') ?>>Aktivitas ringan (Pegawai kantor, Ibu Rumah Tangga, Pegawai Toko, dll)</option>
                            <option value="1.4" <?php echo ($data[0]->f_aktivitas == '1.4' ? 'selected' : '') ?>>Aktivitas sedang (Mahasiswa, pegawai pabrik, Dll)</option>
                            <option value="1.5" <?php echo ($data[0]->f_aktivitas == '1.5' ? 'selected' : '') ?>>Aktivitas berat (Sopir, Kuli, tukang becak, Tukang bangunan, dll)</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lutut">Faktor Stress</label>
                    <div class="input-group">
                        <select name="stress" id="stress" class="form-control" onchange="rubahstress(this)">
                            <option value="">Pilih</option>
                            <option value="13" <?php echo ($data[0]->f_stress == '13' ? 'selected' : '') ?>>Tidak ada stress, pasien dalam kondisi gizi baik</option>
                            <option value="1" <?php echo ($data[0]->f_stress == '1' ? 'selected' : '') ?>>Gagal jantung, bedah minor</option>
                            <option value="2" <?php echo ($data[0]->f_stress == '2' ? 'selected' : '') ?>>Kenaikan suhu tubuh 1C</option>
                            <option value="3" <?php echo ($data[0]->f_stress == '3' ? 'selected' : '') ?>>Trauma skeletal, curettage, PEB, post partum</option>
                            <option value="4" <?php echo ($data[0]->f_stress == '4' ? 'selected' : '') ?>>Operasi besar abdomen/thorax, SCTP</option>
                            <option value="5" <?php echo ($data[0]->f_stress == '5' ? 'selected' : '') ?>>Trauma multiple</option>
                            <option value="6" <?php echo ($data[0]->f_stress == '6' ? 'selected' : '') ?>>Gagal hati, kanker</option>
                            <option value="7" <?php echo ($data[0]->f_stress == '7' ? 'selected' : '') ?>>Sepsis</option>
                            <option value="8" <?php echo ($data[0]->f_stress == '8' ? 'selected' : '') ?>>Pasca operasi selektif (ada alat yang dipasang)</option>
                            <option value="9" <?php echo ($data[0]->f_stress == '9' ? 'selected' : '') ?>>Infeksi</option>
                            <option value="10" <?php echo ($data[0]->f_stress == '10' ? 'selected' : '') ?>>Luka bakar 10%</option>
                            <option value="11" <?php echo ($data[0]->f_stress == '11' ? 'selected' : '') ?>>Luka bakar 25%</option>
                            <option value="12" <?php echo ($data[0]->f_stress == '12' ? 'selected' : '') ?>>Luka bakar 50%</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="lutut">Nilai Stress</label>
                    <div class="input-group">
                        <select name="nilai" id="nilai" class="form-control">
                            <option value="">Pilih</option>
                            <?php foreach ($nilai_stress as $n) : ?>
                                <option value="<?php echo $n ?>" <?php echo ($data[0]->n_stress == $n ? 'selected' : '') ?>><?php echo $n ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <button name="button" type="button" class="btn btn-primary" onclick="hitung2()">Hitung Kebutuhan Gizi</button>
                </div>
            </div>

            <div id="critical" style="display: <?php echo $data[0]->kondisi == '2' ? 'block' : 'none' ?>;">
                <label for="lutut">Jenis</label>
                <div class="input-group">
                    <select name="kkal" id="kkal" class="form-control">
                        <?php for ($i = 12; $i < 31; $i++) {
                        ?>
                            <option value="<?php echo $i ?>" <?php echo ($data[0]->kkal == $i ? 'selected' : '') ?>><?php echo $i ?> kkal/kg BBI</option>
                        <?php
                        } ?>
                        <!-- <option value="12">12 kkal/kg BBI</option>
                                <option value="2">30 kkal/kg BBI</option> -->
                    </select>
                </div>
                <br>
                <div class="form-group">
                    <button name="button" type="button" class="btn btn-primary" onclick="hitung3()">Hitung Kebutuhan Gizi</button>
                </div>
            </div>

        </div>
        <div class="col-md-12" style="margin-top: 3%;display:block" id="div3">
            <center>
                <h5>Hasil</h5>
            </center>
            <table style="font-size: 20px;color:#000;">
                <tr>
                    <td>BMR</td>
                    <td style="width: 20%;text-align:center"> : </td>
                    <td> <span id="bmr"><?php echo $data[0]->bmr ?></span>
                        <input type="hidden" name="bmr" id="bmr_" value="<?php echo $data[0]->bmr ?>">
                    </td>
                    <td style="width: 3%;"></td>
                    <td>Protein</td>
                    <td>:</td>
                    <td>
                        <div class="input-group">
                            <input id="protein" name="protein" type="text" class="form-control" value="<?php echo $data[0]->protein ?>" onkeyup="Desimal(this);Gizi()">
                            <div class="input-group-append">
                                <div class="input-group-text">%</div>
                            </div>
                        </div>
                    </td>
                    <td>=</td>
                    <td>
                        <div class="input-group">
                            <input id="h_protein" name="h_protein" type="text" class="form-control" value="<?php echo $data[0]->h_protein ?>" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">gram</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>KEBUTUHAN GIZI</td>
                    <td style="width: 20%;text-align:center"> : </td>
                    <td> <span id="keb_gizi"><?php echo $data[0]->kebutuhan_gizi ?></span>
                        <input type="hidden" name="kebgizi" id="kebgizi" value="<?php echo $data[0]->kebutuhan_gizi ?>">
                    </td>
                    <td style="width: 3%;"></td>
                    <td>Lemak</td>
                    <td>:</td>
                    <td>
                        <div class="input-group">
                            <input id="lemak" name="lemak" type="text" class="form-control" value="<?php echo $data[0]->lemak ?>" onkeyup="Desimal(this);Gizi()">
                            <div class="input-group-append">
                                <div class="input-group-text">%</div>
                            </div>
                        </div>
                    </td>
                    <td>=</td>
                    <td>
                        <div class="input-group">
                            <input id="h_lemak" name="h_lemak" type="text" class="form-control" value="<?php echo $data[0]->h_lemak ?>" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">gram</div>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td style="width: 20%;text-align:center"></td>
                    <td>
                    </td>
                    <td style="width: 3%;"></td>
                    <td>Karbohidrat </td>
                    <td>:</td>
                    <td>
                        <div class="input-group">
                            <input id="karbo" name="karbo" type="text" class="form-control" value="<?php echo $data[0]->karbo ?>" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">%</div>
                            </div>
                        </div>
                    </td>
                    <td>=</td>
                    <td>
                        <div class="input-group">
                            <input id="h_karbo" name="h_karbo" type="text" class="form-control" value="<?php echo $data[0]->h_karbo ?>" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">gram</div>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <center>
                <button class="btn btn-info" type="button" onclick="simpan()">Simpan</button>
                <button class="btn btn-warning" type="button" onclick="batal()">Batal</button>
            </center>
        </div>
    </div>

</form>

<?php $this->load->view('template/footer'); ?>
<script>
    function Desimal(obj) {
        a = obj.value;
        var reg = new RegExp(/[0-9]+(?:\.[0-9]{0,2})?/g)
        b = a.match(reg, '');
        if (b == null) {
            obj.value = '';
        } else {
            obj.value = b[0];
        }

    }
    $('#tgl').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#tgl_ukur').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    function hitung() {
        var date = document.getElementById('tgl').value
        const tglArray = date.split("/");
        // console.log(tglArray);
        date = tglArray[1] + '/' + tglArray[0] + '/' + tglArray[2];
        var ulna = document.getElementById('ulna').value
        var jk = document.querySelector('input[name="radio"]:checked').value;
        if (jk === "") {
            alert("Masukkan jenis kelamin");
            return false;
        }
        if (date === "") {
            alert("Masukkan tanggal lahir");
        } else {
            var today = new Date();
            var birthday = new Date(date);
            var year = 0;
            if (today.getMonth() < birthday.getMonth()) {
                year = 1;
            } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
                year = 1;
            }
            var age = today.getFullYear() - birthday.getFullYear() - year;
            // alert(age);
            // alert(ulna);
            var hasil = 0;
            if (ulna == 18.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 145;
                    } else {
                        hasil = 146
                    }
                } else {
                    if (age > 65) {
                        hasil = 140;
                    } else {
                        hasil = 147
                    }
                }
            } else if (ulna == 19) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 146;
                    } else {
                        hasil = 148;
                    }
                } else {
                    if (age > 65) {
                        hasil = 142;
                    } else {
                        hasil = 148;
                    }
                }
            } else if (ulna == 19.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 158
                    } else {
                        hasil = 149
                    }
                } else {
                    if (age > 65) {
                        hasil = 142
                    } else {
                        hasil = 148
                    }
                }
            } else if (ulna == 20) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 149
                    } else {
                        hasil = 151
                    }
                } else {
                    if (age > 65) {
                        hasil = 145
                    } else {
                        hasil = 151
                    }
                }
            } else if (ulna == 20.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 151
                    } else {
                        hasil = 153
                    }
                } else {
                    if (age > 65) {
                        hasil = 147
                    } else {
                        hasil = 152
                    }
                }
            } else if (ulna == 21) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 152
                    } else {
                        hasil = 155
                    }
                } else {
                    if (age > 65) {
                        hasil = 148
                    } else {
                        hasil = 154
                    }
                }
            } else if (ulna == 21.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 154
                    } else {
                        hasil = 157
                    }
                } else {
                    if (age > 65) {
                        hasil = 150
                    } else {
                        hasil = 155
                    }
                }
            } else if (ulna == 22) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 156
                    } else {
                        hasil = 158
                    }
                } else {
                    if (age > 65) {
                        hasil = 152
                    } else {
                        hasil = 156
                    }
                }
            } else if (ulna == 22.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 157
                    } else {
                        hasil = 160
                    }
                } else {
                    if (age > 65) {
                        hasil = 153
                    } else {
                        hasil = 158
                    }
                }
            } else if (ulna == 23) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 159
                    } else {
                        hasil = 162
                    }
                } else {
                    if (age > 65) {
                        hasil = 155
                    } else {
                        hasil = 159
                    }
                }
            } else if (ulna == 23.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 160
                    } else {
                        hasil = 164
                    }
                } else {
                    if (age > 65) {
                        hasil = 156
                    } else {
                        hasil = 161
                    }
                }
            } else if (ulna == 24) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 162
                    } else {
                        hasil = 166
                    }
                } else {
                    if (age > 65) {
                        hasil = 158
                    } else {
                        hasil = 162
                    }
                }
            } else if (ulna == 24.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 163
                    } else {
                        hasil = 167
                    }
                } else {
                    if (age > 65) {
                        hasil = 160
                    } else {
                        hasil = 163
                    }
                }
            } else if (ulna == 25) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 165
                    } else {
                        hasil = 169
                    }
                } else {
                    if (age > 65) {
                        hasil = 161
                    } else {
                        hasil = 165
                    }
                }
            } else if (ulna == 25.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 167
                    } else {
                        hasil = 171
                    }
                } else {
                    if (age > 65) {
                        hasil = 163
                    } else {
                        hasil = 166
                    }
                }
            } else if (ulna == 26) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 168
                    } else {
                        hasil = 173
                    }
                } else {
                    if (age > 65) {
                        hasil = 165
                    } else {
                        hasil = 168
                    }
                }
            } else if (ulna == 26.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 170
                    } else {
                        hasil = 175
                    }
                } else {
                    if (age > 65) {
                        hasil = 166
                    } else {
                        hasil = 169
                    }
                }
            } else if (ulna == 27) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 171
                    } else {
                        hasil = 176
                    }
                } else {
                    if (age > 65) {
                        hasil = 168
                    } else {
                        hasil = 170
                    }
                }
            } else if (ulna == 27.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 173
                    } else {
                        hasil = 178
                    }
                } else {
                    if (age > 65) {
                        hasil = 170
                    } else {
                        hasil = 172
                    }
                }
            } else if (ulna == 28) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 175
                    } else {
                        hasil = 180
                    }
                } else {
                    if (age > 65) {
                        hasil = 171
                    } else {
                        hasil = 173
                    }
                }
            } else if (ulna == 28.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 176
                    } else {
                        hasil = 182
                    }
                } else {
                    if (age > 65) {
                        hasil = 173
                    } else {
                        hasil = 175
                    }
                }
            } else if (ulna == 29) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 178
                    } else {
                        hasil = 184
                    }
                } else {
                    if (age > 65) {
                        hasil = 175
                    } else {
                        hasil = 176
                    }
                }
            } else if (ulna == 29.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 179
                    } else {
                        hasil = 185
                    }
                } else {
                    if (age > 65) {
                        hasil = 176
                    } else {
                        hasil = 177
                    }
                }
            } else if (ulna == 30) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 181
                    } else {
                        hasil = 187
                    }
                } else {
                    if (age > 65) {
                        hasil = 178
                    } else {
                        hasil = 179
                    }
                }
            } else if (ulna == 30.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 182
                    } else {
                        hasil = 189
                    }
                } else {
                    if (age > 65) {
                        hasil = 179
                    } else {
                        hasil = 180
                    }
                }
            } else if (ulna == 31) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 184
                    } else {
                        hasil = 191
                    }
                } else {
                    if (age > 65) {
                        hasil = 181
                    } else {
                        hasil = 181
                    }
                }
            } else if (ulna == 31.5) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 186
                    } else {
                        hasil = 193
                    }
                } else {
                    if (age > 65) {
                        hasil = 183
                    } else {
                        hasil = 183
                    }
                }
            } else if (ulna == 32) {
                if (jk == 'L') {
                    if (age > 65) {
                        hasil = 187
                    } else {
                        hasil = 194
                    }
                } else {
                    if (age > 65) {
                        hasil = 184
                    } else {
                        hasil = 184
                    }
                }
            }
            var bbideal;
            // if (hasil > 150) {
            //     bbideal = (hasil - 100) - (0.1 * (hasil - 100));
            // } else {
            //     bbideal = (hasil - 100);
            // }
            hasil_kuadrat = (hasil / 100) * (hasil / 100);

            if (jk == 'L') {
                bbideal = 22.5 * hasil_kuadrat;
            } else if (jk == 'P') {
                bbideal = 21 * hasil_kuadrat;
            }
            $('#div2').show();
            $('#hasil').val(hasil.toFixed(2));
            $('#usia').val((age));
            $('#bbideal').val(bbideal.toFixed(2));

            // alert(hasil)

        }
    }

    function rubahstress(selectObject) {
        var value = selectObject.value;
        // alert(value);
        var html;
        if (value == 1) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.15">1.15</option>');
            $('#nilai').prepend('<option value="1.2">1.2</option>');
        } else if (value == 2) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.13">1.13</option>');
        } else if (value == 3) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.15">1.13</option>');
            $('#nilai').prepend('<option value="1.20">1.20</option>');
            $('#nilai').prepend('<option value="1.25">1.25</option>');
            $('#nilai').prepend('<option value="1.30">1.30</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
        } else if (value == 4) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.3">1.3</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
            $('#nilai').prepend('<option value="1.40">1.40</option>');
            $('#nilai').prepend('<option value="1.45">1.45</option>');
            $('#nilai').prepend('<option value="1.50">1.50</option>');
        } else if (value == 5) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
            $('#nilai').prepend('<option value="1.40">1.40</option>');
            $('#nilai').prepend('<option value="1.45">1.45</option>');
            $('#nilai').prepend('<option value="1.50">1.50</option>');
            $('#nilai').prepend('<option value="1.55">1.55</option>');
        } else if (value == 6) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.5">1.5</option>');
        } else if (value == 7) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.5">1.5</option>');
            $('#nilai').prepend('<option value="1.55">1.55</option>');
            $('#nilai').prepend('<option value="1.60">1.60</option>');
            $('#nilai').prepend('<option value="1.65">1.65</option>');
            $('#nilai').prepend('<option value="1.70">1.70</option>');
            $('#nilai').prepend('<option value="1.75">1.75</option>');
            $('#nilai').prepend('<option value="1.80">1.80</option>');
        } else if (value == 8) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.1">1.1</option>');
            $('#nilai').prepend('<option value="1.15">1.15</option>');
            $('#nilai').prepend('<option value="1.20">1.20</option>');
            $('#nilai').prepend('<option value="1.25">1.25</option>');
            $('#nilai').prepend('<option value="1.30">1.30</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
            $('#nilai').prepend('<option value="1.40">1.40</option>');
            $('#nilai').prepend('<option value="1.45">1.45</option>');
            $('#nilai').prepend('<option value="1.50">1.50</option>');
        } else if (value == 9) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.20">1.20</option>');
            $('#nilai').prepend('<option value="1.25">1.25</option>');
            $('#nilai').prepend('<option value="1.30">1.30</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
            $('#nilai').prepend('<option value="1.40">1.40</option>');
        } else if (value == 10) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.1">1.1</option>');
            $('#nilai').prepend('<option value="1.15">1.15</option>');
            $('#nilai').prepend('<option value="1.20">1.20</option>');
            $('#nilai').prepend('<option value="1.25">1.25</option>');
        } else if (value == 11) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.25">1.25</option>');
            $('#nilai').prepend('<option value="1.30">1.30</option>');
            $('#nilai').prepend('<option value="1.35">1.35</option>');
            $('#nilai').prepend('<option value="1.40">1.40</option>');
            $('#nilai').prepend('<option value="1.45">1.45</option>');
            $('#nilai').prepend('<option value="1.50">1.50</option>');
        } else if (value == 12) {
            $('#nilai').html('');
            // $('#nilai').prepend('<option value="">Pilih</option>');
            $('#nilai').prepend('<option value="1.50">1.50</option>');
            $('#nilai').prepend('<option value="1.55">1.55</option>');
            $('#nilai').prepend('<option value="1.60">1.60</option>');
            $('#nilai').prepend('<option value="1.65">1.65</option>');
            $('#nilai').prepend('<option value="1.70">1.70</option>');
            $('#nilai').prepend('<option value="1.75">1.75</option>');
            $('#nilai').prepend('<option value="1.80">1.80</option>');
            $('#nilai').prepend('<option value="1.85">1.85</option>');
            $('#nilai').prepend('<option value="1.90">1.90</option>');
            $('#nilai').prepend('<option value="1.95">1.95</option>');
            $('#nilai').prepend('<option value="2">2</option>');
        } else if (value == 13) {
            $('#nilai').html('');
            $('#nilai').prepend('<option value="1.3">1.3</option>');
        }
    }

    function rubahmetode(selectObject) {
        var value = selectObject.value;
        if (value == 1) {
            $('#critical').hide();
            $('#non').show();
        } else {
            $('#critical').show();
            $('#non').hide();
        }
    }

    function hitung2() {
        var usia = document.getElementById('usia').value
        var tb = document.getElementById('hasil').value
        var stress = document.getElementById('stress').value
        var nilai = document.getElementById('nilai').value
        var aktivitas = document.getElementById('aktivitas').value
        var bbideal = document.getElementById('bbideal').value
        var jk = document.querySelector('input[name="radio"]:checked').value;
        if (stress === "") {
            alert("Masukkan faktor stress");
            return false;
        } else {
            var bmr;
            if (jk == 'L') {
                bmr = 66 + (13.5 * parseFloat(bbideal)) + (5 * parseFloat(tb)) - (6.8 * parseFloat(usia))
            } else {
                bmr = 655 + (9.6 * parseFloat(bbideal)) + (1.7 * parseFloat(tb)) - (4.7 * parseFloat(usia))
            }
            gizi = (bmr.toFixed(2)) * parseFloat(aktivitas) * parseFloat(nilai);
            $('#div3').show();
            $('#trbmr').show();
            $('#bmr').html(bmr.toFixed(2));
            $('#bmr_').val(bmr.toFixed(2));
            $('#keb_gizi').html(gizi.toFixed(2));
            $('#kebgizi').val(gizi.toFixed(2));
        }

    }

    function hitung3() {
        var bbideal = document.getElementById('bbideal').value
        var kkal = document.getElementById('kkal').value
        hasil = parseFloat(kkal) * parseFloat(bbideal);
        $('#div3').show();
        $('#trbmr').hide();
        $('#keb_gizi').html(hasil.toFixed(2));
        $('#kebgizi').val(hasil.toFixed(2));
    }

    function simpan() {
        // alert($('input[name="jenis_kelamin"]:checked').val())
        if ($('#pasien').val() == '') {
            alert("Pasien Harus Diisi");
            $('#pasien').focus();
            return false;
        }
        if ($('#norm').val() == '') {
            alert("No.RM Harus Diisi");
            $('#norm').focus();
            return false;
        }
        data = $('#form').serialize();
        swal({
                title: "Apakah anda yakin?",
                text: "Pastikan data yang diinputkan benar!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((simpan) => {
                if (simpan) {
                    $.ajax({
                        type: 'POST',
                        url: "<?php echo base_url(); ?>/pengukuran/simpan_edit",
                        data: data,
                        cache: false,
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            if (data.code == 200) {
                                swal({
                                    title: "Sukses",
                                    text: data.msg,
                                    icon: "success",
                                    button: "Ok",
                                }).then(function() {
                                    window.location.href = '<?= base_url('/ukur/data'); ?>';
                                })
                            } else {
                                swal({
                                    title: "Gagal",
                                    text: data.msg,
                                    icon: "error",
                                    button: "Ok",
                                });
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            swal({
                                title: "Gagal",
                                text: xhr.status,
                                icon: "error",
                                button: "Ok",
                            });
                        }
                    });
                }
            });

    }

    function batal() {
        window.location.href = '<?= base_url('/ukur/data'); ?>';
    }
    function Desimal(obj) {
        a = obj.value;
        var reg = new RegExp(/[0-9]+(?:\.[0-9]{0,2})?/g)
        b = a.match(reg, '');
        if (b == null) {
            obj.value = '';
        } else {
            obj.value = b[0];
        }

    }

    function Gizi() {
        var kkal = parseFloat($('#kebgizi').val());
        // kkal = 1500;
        var protein;
        var lemak;
        var karbo;
        var h_protein = 0;
        var h_lemak = 0;
        var h_karbo = 0;
        if ($('#protein').val() == '') {
            protein = 0;
        } else {
            protein = parseFloat($('#protein').val())
            h_protein = (protein / 100) * kkal;
            h_protein = h_protein / 4;
        }
        if ($('#lemak').val() == '') {
            lemak = 0;
        } else {
            lemak = parseFloat($('#lemak').val())
            h_lemak = (lemak / 100) * kkal;
            h_lemak = h_lemak / 9;
        }

        karbo = (100 - protein - lemak);
        h_karbo = ((100 - protein - lemak) / 100) * kkal;
        h_karbo = h_karbo / 4;

        $('#h_protein').val(Math.abs(h_protein).toFixed(2));
        $('#h_lemak').val(Math.abs(h_lemak).toFixed(2));
        $('#h_karbo').val(Math.abs(h_karbo).toFixed(2));
        $('#karbo').val(Math.abs(karbo).toFixed(2));
    }
</script>