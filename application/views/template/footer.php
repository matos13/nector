</div>
</div>

<script src="<?= base_url('aset/'); ?>js/jquery.min.js"></script>
<script src="<?= base_url('aset/'); ?>js/popper.js"></script>
<script src="<?= base_url('aset/'); ?>js/bootstrap.min.js"></script>
<script src="<?= base_url('aset/'); ?>js/main.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?= base_url('assets/'); ?>js/sb-admin-2.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" integrity="sha512-TQQ3J4WkE/rwojNFo6OJdyu6G8Xe9z8rMrlF9y7xpFbQfW5g8aSWcygCQ4vqRiJqFsDsE1T6MoAOMJkFXlrI9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    // const sidebar = document.querySelector('.sidebar');
    // const mainContent = document.querySelector('.main-content');
    // document.querySelector('button').onclick = function() {
    //     sidebar.classList.toggle('sidebar_small');
    //     mainContent.classList.toggle('main-content_large')
    // }

    function side() {
        const sidebar = document.querySelector('.sidebar2');
        const url = document.querySelector('.url');
        const url2 = document.querySelector('.url2');
        const url3 = document.querySelector('.url3');
        const url4 = document.querySelector('.url4');
        const url5 = document.querySelector('.url5');
        const btn1 = document.querySelector('.btn1');
        const btn2 = document.querySelector('.btn2');
        const btn3 = document.querySelector('.btn3');
        const btn4 = document.querySelector('.btn4');
        const btn5 = document.querySelector('.btn5');
        const mainContent = document.querySelector('.main-content');
        sidebar.classList.toggle('sidebar_small');
        mainContent.classList.toggle('main-content_large')
        url.classList.toggle('url-active')
        url2.classList.toggle('url-active')
        url3.classList.toggle('url-active')
        url4.classList.toggle('url-active')
        url5.classList.toggle('url-active')
        btn1.classList.toggle('btn-active')
        btn2.classList.toggle('btn-active')
        btn3.classList.toggle('btn-active')
        btn4.classList.toggle('btn-active')
        btn5.classList.toggle('btn-active')
    }
</script>