<!doctype html>
<html lang="en">

<head>
	<title>NECTOR</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="icon" type="image/x-icon" href="<?= base_url('assets/img/'); ?>logo.ico">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?= base_url('aset/'); ?>css/style.css">
	<link href="<?= base_url('assets/'); ?>vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
	<link href="<?= base_url('assets/'); ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

	<!-- Custom styles for this template-->
	<link href="<?= base_url('assets/'); ?>css/sb-admin-2.min.css" rel="stylesheet">

</head>
<style>
	.footer {
		position: fixed;
		bottom: 0;
		width: 100%;
	}

	.keluar {
		position: fixed;
		bottom: 0;
		width: 100%;
	}

	.grid-container {
		width: 90vw;
		margin: 0 auto;
		height: 90vh;
		display: flex;
		border: 2px solid rgba(0, 0, 0, .4);
		transition: 1s ease;
	}

	.sidebar2 {
		width: 30%;
		/* height: 100%; */
		min-height: 100vh;
		background: #494ca2;
		-webkit-transition: all 0.3s;
		-o-transition: all 0.3s;
		transition: all 0.3s;
		position: relative;
	}

	.main-content {
		width: 70%;
		height: 100%;
		/* background-color: #ccc;	 */
		transition: 1s ease;
	}

	.main-content_large {
		width: 90%;
	}

	.sidebar_small {
		width: 10%;
	}

	/* button {
		position: absolute;
		border: none;
		height: 40px;
		width: 40px;
		border-radius: 50%;
		box-shadow: 0px 1px 4px 1px rgba(0, 0, 0, .3);
		left: 100%;
		top: 2rem;
		transform: translateX(-50%);
		cursor: pointer;
	} */
	.sidebar2 a {
		color: #fff !important;
	}

	.sidebar2 span {
		color: #fff !important;
	}

	.sidebar2 li {
		margin-bottom: 2%;
	}


	.sidebar2 nav {
		height: 100vh;
	}

	.button5 {
		background-color: wheat;
		padding: 10px;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 16px;
	}

	.button4 {
		background-color: wheat;
		padding: 5%;
		text-align: center;
		text-decoration: none;
		display: inline-block;
		font-size: 10px;
		margin-right: 4%;
	}

	.url-active {
		display: none;
	}

	.btn-active {
		margin-top: 15%;
		margin-left: 30%;
	}
</style>

<body>
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
					<button class="close" type="button" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
				</div>
				<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
				<div class="modal-footer">
					<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
					<a class="btn btn-primary" href="<?= base_url('logout'); ?>">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper d-flex align-items-stretch">
		<div class="sidebar2">

			<nav id="">
				<div class="p-4" style="height: 80%;">
					<center><a href="" style="color:#fff"><img src="<?= base_url('assets/img/logo.png'); ?>" alt="" style="width: 100%; margin-right:3%"></a></center>
					<ul class="list-unstyled components mb-5" style="margin-top: 10%;">
						<li class="">
							<a href="<?= base_url('ukur'); ?>"><button type="button" class="rounded-circle border-0 button4 btn1"><img src="<?= base_url('aset/leg.png'); ?>" style="width: 20px;" /></button><span class="url">Tinggi Lutut</span></a>
						</li>
						<li>
							<a href="<?= base_url('ukur/ulna'); ?>"><button type="button" class="rounded-circle border-0 button4 btn2"><img src="<?= base_url('aset/arm.png'); ?>" style="width: 20px;" /></button><span class="url2">Panjang Ulna</span></a>
							<!-- <a href="<?= base_url('ukur/ulna'); ?>"><span class="fa fa-user mr-3"></span> Panjang Ulna</a> -->
						</li>
						<li>
							<a href="<?= base_url('ukur/laporan'); ?>"><button type="button" class="rounded-circle border-0 button4 btn3"><img src="<?= base_url('aset/report.png'); ?>" style="width: 20px;" /></button><span class="url3">Laporan</span></a>
							<!-- <a href="<?= base_url('ukur/laporan'); ?>"><span class="fa fa-file-pdf-o mr-3"></span> Laporan</a> -->
						</li>
						<li>
							<a href="<?= base_url('ukur/data'); ?>"><button type="button" class="rounded-circle border-0 button4 btn4"><img src="<?= base_url('aset/data.png'); ?>" style="width: 20px;" /></button><span class="url4">Data Pasien</span></a>
							<!-- <a href="<?= base_url('ukur/data'); ?>"><span class="fa fa-user mr-3"></span> Data Pasien</a> -->
						</li>
						<li>
							<a href="#" data-toggle="modal" data-target="#logoutModal"><button type="button" class="rounded-circle border-0 button4 btn5"><img src="<?= base_url('aset/switch.png'); ?>" style="width: 20px;" /></button><span class="url5">Keluar</span></a>
							<!-- <a href="<?= base_url('ukur/data'); ?>"><span class="fa fa-user mr-3"></span> Data Pasien</a> -->
						</li>
					</ul>
					<div>
						<center>
							<button type="button" class="rounded-circle border-0 button5" onclick="side()"><span style="margin: 5px;" class="fa fa-arrow-circle-left"></span></button>
							<!-- <a href="#" onclick="side()"></a> -->
							<!-- <button class="rounded-circle border-0" id="sidebarToggle"></button> -->
						</center>
					</div>
					<div class="footer">
						<!-- <ul class="list-unstyled components">
							<li>
								<a href="#" data-toggle="modal" data-target="#logoutModal"><span class="fa fa-power-off mr-3"></span>Keluar</a>
							</li>
						</ul> -->
						<span style="font-size: 12px;text-align:center">INSTALASI GIZI <br> RSUD DR. SAIFUL ANWAR MALANG</span>
					</div>

				</div>
			</nav>
		</div>
		<div id="content" class="p-4 p-md-5 pt-5 main-content" style="background-color: wheat;padding-top:1% !important">
			<div style="width: 100%;">

				<table style="width:100% ;margin-bottom:2%" border="0">
					<tr>
						<td style="width: 20%;" align="center">
							<img src="<?= base_url('assets/img/rssa.png'); ?>" style="width:100px">
						</td>
						<td>
							<center>
								<h5 class="mb-4"> NECTOR <br>(Nutritional Needs Calculator)</h5>
							</center>
						</td>
						<td style="width: 20%;" align="center">
							<img src="<?= base_url('assets/img/gizi.png'); ?>" style="width:90px">
						</td>
					</tr>
				</table>
			</div>