<!doctype html>
<html lang="en">

<head>
    <title>NECTOR</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <div class="wrapper d-flex align-items-stretch">
        <nav id="sidebar">
            <div class="custom-menu">
                <button type="button" id="sidebarCollapse" class="btn btn-primary">
                    <i class="fa fa-bars"></i>
                    <span class="sr-only">Toggle Menu</span>
                </button>
            </div>
            <div class="p-4">
                <h1 style="margin-bottom: 0px;"><a href="index.php" class="logo">NECTOR</a></h1>
                <span>(Nutritional Needs Calculator)</span>
                <ul class="list-unstyled components mb-5" style="margin-top: 2%;">
                    <li class="active">
                        <a href="index.php"><span class="fa fa-area-chart mr-3"></span> Tinggi Lutut</a>
                    </li>
                    <li>
                        <a href="ulna.php"><span class="fa fa-area-chart mr-3"></span> Panjang Ulna</a>
                    </li>
                </ul>
                <div class="footer">
                    <p>
                        Copyright &copy;
                        <script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Mada</a>
                    </p>
                </div>

            </div>
        </nav>

        <!-- Page Content  -->
        <div id="content" class="p-4 p-md-5 pt-5" style="background-color: wheat;">
            <center>
                <h5 class="mb-4"> NECTOR <br>(Nutritional Needs Calculator)</h5>
            </center>
            <span style="color: #000;">PERHITUNGAN ESTIMASI TINGGI BADAN MENGGUNAKAN TINGGI LUTUT</span>
            <br>
            <div class="row" style="margin-top: 2%;">
                <div class="col-md-6">
                    <form>
                        <div class="form-group">
                            <label>Jenis Kelamin</label>
                            <div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input name="radio" id="radio_0" type="radio" class="custom-control-input" value="L" checked>
                                    <label for="radio_0" class="custom-control-label">Laki-laki</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input name="radio" id="radio_1" type="radio" class="custom-control-input" value="P">
                                    <label for="radio_1" class="custom-control-label">Perempuan</label>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="bb">Berat Badan</label>
                            <div class="input-group">
                                <input id="bb" name="bb" type="text" class="form-control" onkeyup="Desimal(this)">
                                <div class="input-group-append">
                                    <div class="input-group-text">Kg</div>
                                </div>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label for="tgl">Tanggal Lahir</label>
                            <div class="input-group">
                                <input id="tgl" name="tgl" type="text" class="form-control">
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar-o"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lutut">Tinggi Lutut</label>
                            <div class="input-group">
                                <input id="lutut" name="lutut" type="text" class="form-control" onkeyup="Desimal(this)">
                                <div class="input-group-append">
                                    <div class="input-group-text">Cm</div>
                                </div>
                            </div>
                        </div>

                    </form>
                    <div class="form-group">
                        <button name="button" type="submit" class="btn btn-primary" onclick="hitung()">Hitung</button>
                    </div>
                    <div class="form-group">
                        <label for="bbideal">Berat Badan Ideal</label>
                        <div class="input-group">
                            <input id="bbideal" name="bbideal" type="text" class="form-control" readonly>
                            <div class="input-group-append">
                                <div class="input-group-text">Kg</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="hasil">Estimasi Tinggi Badan</label>
                        <input id="hasil" name="hasil" type="text" class="form-control" readonly>
                        <input type="hidden" name="usia" id="usia">
                    </div>
                </div>
                <div class="col-md-6" style="display: none;" id="div2">
                    <div>
                        <div class="form-group">
                            <label for="lutut">Metode</label>
                            <div class="input-group">
                                <select name="metode" id="metode" class="form-control" onchange="rubahmetode(this)">
                                    <option value="1">Non - Critical Ill Patients</option>
                                    <option value="2">Critical Ill Patients</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div id="non" style="display: block;">
                        <div class="form-group">
                            <label for="lutut">Faktor Aktivitas</label>
                            <div class="input-group">
                                <select name="aktivitas" id="aktivitas" class="form-control">
                                    <option value="1.05">Total Bed Rest, CVA-ICH</option>
                                    <option value="1.1">Mobilisasi di tempat tidur</option>
                                    <option value="1.2">Jalan di sekitar kamar</option>
                                    <option value="1.3">Aktivitas ringan (Pegawai kantor, Ibu Rumah Tangga, Pegawai Toko, dll)</option>
                                    <option value="1.4">Aktivitas sedang (Mahasiswa, pegawai pabrik, Dll)</option>
                                    <option value="1.5">Aktivitas berat (Sopir, Kuli, tukang becak, Tukang bangunan, dll)</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lutut">Faktor Stress</label>
                            <div class="input-group">
                                <select name="stress" id="stress" class="form-control" onchange="rubahstress(this)">
                                    <option value="">Pilih</option>
                                    <option value="13">Tidak ada stress, pasien dalam kondisi gizi baik</option>
                                    <option value="1">Gagal jantung, bedah minor</option>
                                    <option value="2">Kenaikan suhu tubuh 1C</option>
                                    <option value="3">Trauma skeletal, curettage, PEB, post partum</option>
                                    <option value="4">Operasi besar abdomen/thorax, SCTP</option>
                                    <option value="5">Trauma multiple</option>
                                    <option value="6">Gagal hati, kanker</option>
                                    <option value="7">Sepsis</option>
                                    <option value="8">Pasca operasi selektif (ada alat yang dipasang)</option>
                                    <option value="9">Infeksi</option>
                                    <option value="10">Luka bakar 10%</option>
                                    <option value="11">Luka bakar 25%</option>
                                    <option value="12">Luka bakar 50%</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lutut">Nilai Stress</label>
                            <div class="input-group">
                                <select name="nilai" id="nilai" class="form-control">
                                    <option value="">Pilih</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button name="button" type="submit" class="btn btn-primary" onclick="hitung2()">Hitung Kebutuhan Gizi</button>
                        </div>
                    </div>
                    <div id="critical" style="display:none ;">
                        <label for="lutut">Jenis</label>
                        <div class="input-group">
                            <select name="kkal" id="kkal" class="form-control">
                                <?php for ($i = 12; $i < 31; $i++) {
                                ?>
                                    <option value="<?php echo $i ?>"><?php echo $i ?> kkal/kg BBI</option>
                                <?php
                                } ?>
                                <!-- <option value="12">12 kkal/kg BBI</option>
                                <option value="2">30 kkal/kg BBI</option> -->
                            </select>
                        </div>
                        <br>
                        <div class="form-group">
                            <button name="button" type="submit" class="btn btn-primary" onclick="hitung3()">Hitung Kebutuhan Gizi</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" style="margin-top: 3%;display:none" id="div3">
                    <center>
                        <h5>Hasil</h5>
                    </center>
                    <table style="font-size: 20px;color:#000;">
                        <tr id="trbmr">
                            <td>BMR</td>
                            <td style="width: 20%;text-align:center"> : </td>
                            <td> <span id="bmr"></span></td>
                        </tr>
                        <tr>
                            <td>KEBUTUHAN GIZI</td>
                            <td style="width: 20%;text-align:center"> : </td>
                            <td> <span id="keb_gizi"></span></td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css" integrity="sha512-TQQ3J4WkE/rwojNFo6OJdyu6G8Xe9z8rMrlF9y7xpFbQfW5g8aSWcygCQ4vqRiJqFsDsE1T6MoAOMJkFXlrI9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />


    <script>
        function Desimal(obj) {
            a = obj.value;
            var reg = new RegExp(/[0-9]+(?:\.[0-9]{0,2})?/g)
            b = a.match(reg, '');
            if (b == null) {
                obj.value = '';
            } else {
                obj.value = b[0];
            }

        }
        $('#tgl').datepicker({
            format: 'dd/mm/yyyy'
        });

        function hitung() {
            var date = document.getElementById('tgl').value
            const tglArray = date.split("/");
            // console.log(tglArray);
            date = tglArray[1] + '/' + tglArray[0] + '/' + tglArray[2];
            // var bb = document.getElementById('bb').value
            var lutut = document.getElementById('lutut').value
            var jk = document.querySelector('input[name="radio"]:checked').value;
            if (jk === "") {
                alert("Masukkan jenis kelamin");
                return false;
            }
            if (lutut === "") {
                alert("Masukkan tinggi lutut");
                return false;
            }
            if (date === "") {
                alert("Masukkan tanggal lahir");
            } else {
                var today = new Date();
                var birthday = new Date(date);
                var year = 0;
                if (today.getMonth() < birthday.getMonth()) {
                    year = 1;
                } else if ((today.getMonth() == birthday.getMonth()) && today.getDate() < birthday.getDate()) {
                    year = 1;
                }
                var age = today.getFullYear() - birthday.getFullYear() - year;
                // alert(birthday);
                var hasil;
                var bbideal;

                if (jk == 'L') {
                    hasil = (2.02 * parseFloat(lutut)) - (0.04 * age) + 64.19;

                    hasil_kuadrat = (hasil / 100) * (hasil / 100);
                    bbideal = 22.5 * hasil_kuadrat;
                } else if (jk == 'P') {
                    hasil = (1.83 * parseFloat(lutut)) - (0.24 * age) + 84.88;

                    hasil_kuadrat = (hasil / 100) * (hasil / 100);
                    bbideal = 21 * hasil_kuadrat;
                }

                // if (hasil > 150) {
                //     bbideal = (hasil - 100) - (0.1 * (hasil - 100));
                // } else {
                //     bbideal = (hasil - 100);
                // }
                // console.log('lutut='+lutut)
                // console.log('age='+age)
                $('#div2').show();
                $('#hasil').val(hasil.toFixed(2));
                $('#bbideal').val(bbideal.toFixed(2));
                $('#usia').val((age));
                // alert(hasil)

            }
        }

        function rubahstress(selectObject) {
            var value = selectObject.value;
            var html;
            if (value == 1) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.15">1.15</option>');
                $('#nilai').prepend('<option value="1.2">1.2</option>');
            } else if (value == 2) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.13">1.13</option>');
            } else if (value == 3) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.15">1.13</option>');
                $('#nilai').prepend('<option value="1.20">1.20</option>');
                $('#nilai').prepend('<option value="1.25">1.25</option>');
                $('#nilai').prepend('<option value="1.30">1.30</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
            } else if (value == 4) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.3">1.3</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
                $('#nilai').prepend('<option value="1.40">1.40</option>');
                $('#nilai').prepend('<option value="1.45">1.45</option>');
                $('#nilai').prepend('<option value="1.50">1.50</option>');
            } else if (value == 5) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
                $('#nilai').prepend('<option value="1.40">1.40</option>');
                $('#nilai').prepend('<option value="1.45">1.45</option>');
                $('#nilai').prepend('<option value="1.50">1.50</option>');
                $('#nilai').prepend('<option value="1.55">1.55</option>');
            } else if (value == 6) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.5">1.5</option>');
            } else if (value == 7) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.5">1.5</option>');
                $('#nilai').prepend('<option value="1.55">1.55</option>');
                $('#nilai').prepend('<option value="1.60">1.60</option>');
                $('#nilai').prepend('<option value="1.65">1.65</option>');
                $('#nilai').prepend('<option value="1.70">1.70</option>');
                $('#nilai').prepend('<option value="1.75">1.75</option>');
                $('#nilai').prepend('<option value="1.80">1.80</option>');
            } else if (value == 8) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.1">1.1</option>');
                $('#nilai').prepend('<option value="1.15">1.15</option>');
                $('#nilai').prepend('<option value="1.20">1.20</option>');
                $('#nilai').prepend('<option value="1.25">1.25</option>');
                $('#nilai').prepend('<option value="1.30">1.30</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
                $('#nilai').prepend('<option value="1.40">1.40</option>');
                $('#nilai').prepend('<option value="1.45">1.45</option>');
                $('#nilai').prepend('<option value="1.50">1.50</option>');
            } else if (value == 9) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.20">1.20</option>');
                $('#nilai').prepend('<option value="1.25">1.25</option>');
                $('#nilai').prepend('<option value="1.30">1.30</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
                $('#nilai').prepend('<option value="1.40">1.40</option>');
            } else if (value == 10) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.1">1.1</option>');
                $('#nilai').prepend('<option value="1.15">1.15</option>');
                $('#nilai').prepend('<option value="1.20">1.20</option>');
                $('#nilai').prepend('<option value="1.25">1.25</option>');
            } else if (value == 11) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.25">1.25</option>');
                $('#nilai').prepend('<option value="1.30">1.30</option>');
                $('#nilai').prepend('<option value="1.35">1.35</option>');
                $('#nilai').prepend('<option value="1.40">1.40</option>');
                $('#nilai').prepend('<option value="1.45">1.45</option>');
                $('#nilai').prepend('<option value="1.50">1.50</option>');
            } else if (value == 12) {
                $('#nilai').html('');
                // $('#nilai').prepend('<option value="">Pilih</option>');
                $('#nilai').prepend('<option value="1.50">1.50</option>');
                $('#nilai').prepend('<option value="1.55">1.55</option>');
                $('#nilai').prepend('<option value="1.60">1.60</option>');
                $('#nilai').prepend('<option value="1.65">1.65</option>');
                $('#nilai').prepend('<option value="1.70">1.70</option>');
                $('#nilai').prepend('<option value="1.75">1.75</option>');
                $('#nilai').prepend('<option value="1.80">1.80</option>');
                $('#nilai').prepend('<option value="1.85">1.85</option>');
                $('#nilai').prepend('<option value="1.90">1.90</option>');
                $('#nilai').prepend('<option value="1.95">1.95</option>');
                $('#nilai').prepend('<option value="2">2</option>');
            } else if (value == 13) {
                $('#nilai').html('');
                $('#nilai').prepend('<option value="1.3">1.3</option>');
            }
        }

        function rubahmetode(selectObject) {
            var value = selectObject.value;
            if (value == 1) {
                $('#critical').hide();
                $('#non').show();
            } else {
                $('#critical').show();
                $('#non').hide();
            }
        }

        function hitung2() {
            var usia = document.getElementById('usia').value
            var tb = document.getElementById('hasil').value
            var stress = document.getElementById('stress').value
            var nilai = document.getElementById('nilai').value
            var aktivitas = document.getElementById('aktivitas').value
            var bbideal = document.getElementById('bbideal').value
            var jk = document.querySelector('input[name="radio"]:checked').value;
            if (stress === "") {
                alert("Masukkan faktor stress");
                return false;
            } else {
                var bmr;
                if (jk == 'L') {
                    bmr = 66 + (13.5 * parseFloat(bbideal)) + (5 * parseFloat(tb)) - (6.8 * parseFloat(usia))
                } else {
                    bmr = 655 + (9.6 * parseFloat(bbideal)) + (1.7 * parseFloat(tb)) - (4.7 * parseFloat(usia))
                }
                gizi = (bmr.toFixed(2)) * parseFloat(aktivitas) * parseFloat(nilai);
                $('#div3').show();
                $('#trbmr').show();
                $('#bmr').html(bmr.toFixed(2));
                $('#keb_gizi').html(gizi.toFixed(2));
            }

        }

        function hitung3() {
            var bbideal = document.getElementById('bbideal').value
            var kkal = document.getElementById('kkal').value
            // console.log(bbideal);
            // console.log(kkal);
            hasil = parseFloat(kkal) * parseFloat(bbideal);
            // if (kkal == 1) {
            //     hasil = 25 * parseFloat(bbideal);
            // } else {
            //     hasil = 30 * parseFloat(bbideal);
            // }
            $('#div3').show();
            $('#trbmr').hide();
            $('#keb_gizi').html(hasil.toFixed(2));
        }
    </script>
</body>

</html>